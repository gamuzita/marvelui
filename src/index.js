import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import {ApolloProvider} from '@apollo/react-hooks';
import client from "./apolloClient";

import store from './store';

const app = (
    <Provider store={store}>
          <ApolloProvider client={client}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
        </ApolloProvider>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
