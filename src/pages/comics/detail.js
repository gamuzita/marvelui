import React from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col,
  Table,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useParams } from "react-router-dom";
import { addItem, removeItem, verifyItem } from "./../../localstorage";

export const GET_COMIC_DETAIL = gql`
  query getDetail($id: ID!) {
    comic(id: $id) {
        id
        title
        description
        thumbnail
        issueNumber
        format
      characters {
        id
      name
      description
      thumbnail
      }
      stories {
        id
        title
        description
        thumbnail
      }
    }
  }
`;

const Comic = () => {
  const { id } = useParams();
  const { loading, data } = useQuery(GET_COMIC_DETAIL, {
    variables: { id },
  });

  if (loading) {
    return <div>Loading...</div>;
  }

  const { comic } = data;

  console.log(comic);
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <Card>
                <CardBody>
                  <Row>
                    <Col xl={5}>
                      <div className="product-detail">
                        <Row>
                          <Col md={12} xs={12}>
                            <div className="product-img">
                              <img
                                src={comic.thumbnail}
                                id="expandedImg1"
                                alt=""
                                className="img-fluid mx-auto d-block"
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col xl={7}>
                      <div className="mt-4 mt-xl-3">
                        <Link to="#" className="text-primary">
                          {comic.title}
                        </Link>
                        <h5 className="mt-1 mb-3">{comic.description}</h5>
                      </div>
                      <div className="text-center mt-2">
                        <Col sm={2}>
                          {verifyItem('comic', comic.id)? 
                            <Button onClick={()=>removeItem('comic', comic)} color="warning" block type="button" className="waves-effect waves-light">
                                <i className="mdi mdi-heart mr-1"></i>  
                            </Button>
                            :
                            <Button onClick={()=>addItem('comic', comic)} color="light" block type="button" className="waves-effect waves-light">
                                <i className="mdi mdi-heart mr-1"></i>
                            </Button>
                            }
                        </Col>
                    </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={6}>
                      <Card className="checkout-order-summary">
                        <CardBody>
                          <div className="p-3 bg-light mb-4">
                            <h5 className="font-size-14 mb-0">Characters</h5>
                          </div>
                          <div className="table-responsive">
                            <Table className="table-centered mb-0 table-nowrap">
                              <tbody>
                                {comic.characters.map((character, index) => {
                                  return (
                                    <tr key={index}>
                                      <th scope="row">
                                      <Link
                                            to={"/character/" + character.id}
                                            className="text-dark"
                                          >
                                        <img
                                          src={
                                            character.thumbnail === ""
                                              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                              : character.thumbnail
                                          }
                                          alt="product-img"
                                          title="product-img"
                                          className="avatar-md"
                                        />
                                        </Link>
                                      </th>
                                      <td>
                                        <h5 className="font-size-14 text-truncate">
                                          <Link
                                            to={"/character/" + character.id}
                                            className="text-dark"
                                          >
                                            {character.name}
                                          </Link>
                                        </h5>
                                        <p className="text-muted mb-0">
                                          {character.description}
                                        </p>
                                      </td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </Table>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col lg={6}>
                      <Card className="checkout-order-summary">
                        <CardBody>
                          <div className="p-3 bg-light mb-4">
                            <h5 className="font-size-14 mb-0">Stories</h5>
                          </div>
                          <div className="table-responsive">
                            <Table className="table-centered mb-0 table-nowrap">
                              <tbody>
                                {comic.stories.map((story, index) => {
                                  return (
                                    <tr key={index}>
                                      <th scope="row">
                                      <Link
                                            to={"/story/" + story.id}
                                            className="text-dark"
                                          >
                                        <img
                                          src={
                                            story.thumbnail === ""
                                              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                              : story.thumbnail
                                          }
                                          alt="product-img"
                                          title="product-img"
                                          className="avatar-md"
                                        />
                                        </Link>
                                      </th>
                                      <td>
                                        <h5 className="font-size-14 text-truncate">
                                          <Link
                                            to={"/story/" + story.id}
                                            className="text-dark"
                                          >
                                            {story.title}
                                          </Link>
                                        </h5>
                                        <p className="text-muted mb-0">
                                          {story.description}
                                        </p>
                                      </td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </Table>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Comic;
