import React, { useState } from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col,
  Input,
} from "reactstrap";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import InfiniteScroll from "react-infinite-scroll-component";
import { addItem, removeItem, verifyItem } from "./../../localstorage";


export const GET_COMICS = gql`
  query getComics($offset: Int, $limit: Int, $search: String) {
    comics(
      pagination: { offset: $offset, limit: $limit },
      filter: {
        titleStartsWith: $search
      }
    )
    {
      offset
      total
      limit
      count
      results {
        id
        title
        description
        thumbnail
        issueNumber
        format
      }
    }
  }
`;

const LIMIT = 20;

const Comics = () => {
  const [sortByType, setSort] = useState(0);
  const [search, setsearch] = useState();
  const { loading, data, fetchMore } = useQuery(GET_COMICS, {
    variables: {
      limit: LIMIT,
      desc: sortByType,
      search: search
    },
  });

  if (loading) {
    return <div>loading</div>;
  }

  const loadMore = () => {
    fetchMore({
      variables: {
        offset: comics.length,
        limit: LIMIT,
        desc: sortByType,
        search: search
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        const prevComics = prev?.comics?.results || [];
        const newComics = fetchMoreResult?.comics?.results || [];

        const data = {
          comics: {
            ...fetchMoreResult.comics,
            results: [...prevComics, ...newComics],
          },
        };

        return data;
      },
    });
  };

  const handleKeyDown=(e)=> {
    if (e.key === 'Enter') {
      setsearch(e.target.value);
      loadMore(true);
    }
  }

  const comics = data?.comics?.results || [];

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <Card>
                <CardBody>
                  <div>
                    <Row>
                      <Col md={6}>
                        <ul className="list-inline my-3 ecommerce-sortby-list">
                          <li className="list-inline-item">
                            <span className="font-weight-medium font-family-secondary">
                              Ordenar por:
                            </span>
                          </li>
                          <li
                            className={
                              "list-inline-item ml-1 " +
                              (sortByType === 0 ? "active" : "")
                            }
                          >
                            <Link
                              to="#"
                              onClick={() => {
                                setSort(0);
                                console.log("click false");
                              }}
                            >
                              TITLE
                            </Link>
                          </li>
                          <li
                            className={
                              "list-inline-item ml-1 " +
                              (sortByType === 1 ? "active" : "")
                            }
                          >
                            <Link to="#" 
                              onClick={() => setSort(1)}>
                              ISSUE NUMBER
                            </Link>
                          </li>
                        </ul>
                      </Col>

                      <Col md={6}>
                        <div className="form-inline float-md-right">
                          <div className="search-box ml-2">
                            <div className="position-relative">
                              <Input
                                type="input"
                                className="form-control rounded"
                                placeholder="Buscar por title..."
                                onKeyDown={handleKeyDown}
                              />
                              <i className="mdi mdi-magnify search-icon"></i>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>

                    <InfiniteScroll
                      dataLength={comics.length}
                      next={loadMore}
                      hasMore={comics.length < data?.comics?.total}
                      loader={<div>loading...</div>}
                      height={"72vh"}
                    >
                      <Row className="no-gutters">
                        {comics.map((item, index) => {
                          return (
                            <Col xl={4} sm={6} key={index}>
                              <div className="product-box">
                                <div className="product-img">
                                  <div className="product-like">
                                  {
                                      verifyItem('comic', item.id)? 
                                        <Link to="#" onClick={()=>removeItem('comic', item)}>
                                          <i className="mdi mdi-heart text-warning"></i>
                                        </Link>
                                        :
                                        <Link to="#" onClick={()=>addItem('comic', item)}>
                                          <i className="mdi mdi-heart-outline"></i>
                                        </Link>
                                    }
                                  </div>
                                  <Link to={"comic/" + item.id}>
                                  <img
                                    src={item.thumbnail}
                                    alt={item.title}
                                    className="img-fluid mx-auto d-block"
                                  />
                                  </Link>
                                </div>

                                <div className="text-center">
                                  <p className="font-size-12 mb-1">
                                    {item.issueNumber} - {item.title}
                                  </p>
                                  <h5 className="font-size-15">
                                    <Link to="#" className="text-dark">
                                      {item.description}
                                    </Link>
                                  </h5>

                                  <h5 className="mt-3 mb-0"><span className="text-muted mr-2"></span>{item.format}</h5>
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                      </Row>
                    </InfiniteScroll>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Comics;
