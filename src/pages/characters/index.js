import React, { useState } from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col,
  Input
} from "reactstrap";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import InfiniteScroll from "react-infinite-scroll-component";

import { addItem, removeItem, verifyItem } from "./../../localstorage";

export const GET_CHARACTERS = gql`
  query getCharacters($offset: Int, $limit: Int, $desc: Boolean, $search: String) {
    characters(
      pagination: { offset: $offset, limit: $limit }
      desc: $desc,
      filter: {
          nameStartsWith: $search
      }
    ) 
    {
      offset
      total
      limit
      count
      results {
        id
        name
        description
        thumbnail
      }
    }
  }
`;

const LIMIT = 20;

const Characters = () => {
  const [sortAscending, setSort] = useState(false);
  const [search, setsearch] = useState();
  const { loading, data, fetchMore } = useQuery(GET_CHARACTERS, {
    variables: {
      limit: LIMIT,
      desc: sortAscending,
      search: search
    },
  });

  if (loading) {
    return <div>loading</div>;
  }

  const loadMore = (isSearching) => {
    fetchMore({
      variables: {
        offset: isSearching? 0 : characters.length,
        limit: LIMIT,
        desc: sortAscending,
        search: search
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        const prevCharacters = prev?.characters?.results || [];
        const newCharacters = fetchMoreResult?.characters?.results || [];

        const data = {
          characters: {
            ...fetchMoreResult.characters,
            results: [...prevCharacters, ...newCharacters],
          },
        };

        return data;
      },
    });
  };


  const handleKeyDown=(e)=> {
    if (e.key === 'Enter') {
      setsearch(e.target.value);
      loadMore(true);
    }
  }

  const characters = data?.characters?.results || [];

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <Card>
                <CardBody>
                  <div>
                    <Row>
                      <Col md={6}>
                        <ul className="list-inline my-3 ecommerce-sortby-list">
                          <li className="list-inline-item">
                            <span className="font-weight-medium font-family-secondary">
                              Ordenar por nombre:
                            </span>
                          </li>
                          <li
                            className={
                              "list-inline-item ml-1 " +
                              (sortAscending === true ? "" : "active")
                            }
                          >
                            <Link
                              to="#"
                              onClick={() => {
                                setSort(false);
                                console.log("click false");
                              }}
                            >
                              Ascendente
                            </Link>
                          </li>
                          <li
                            className={
                              "list-inline-item ml-1 " +
                              (sortAscending === true ? "active" : "")
                            }
                          >
                            <Link to="#" onClick={() => setSort(true)}>
                              Descendente
                            </Link>
                          </li>
                        </ul>
                      </Col>

                      <Col md={6}>
                        <div className="form-inline float-md-right">
                          <div className="search-box ml-2">
                            <div className="position-relative">
                              <Input
                                type="input"
                                className="form-control rounded"
                                placeholder="Buscar por nombre..."
                                onKeyDown={handleKeyDown}
                              />
                              <i className="mdi mdi-magnify search-icon"></i>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>

                    <InfiniteScroll
                      dataLength={characters.length}
                      next={loadMore}
                      hasMore={characters.length < data?.characters?.total}
                      loader={<div>loading...</div>}
                      height={"72vh"}
                    >
                      <Row className="no-gutters">
                        {characters.map((item, index) => {
                          return (
                            <Col xl={4} sm={6} key={index}>
                              <div className="product-box">
                                <div className="product-img">
                                  <div className="product-like">
                                    {
                                      verifyItem('character', item.id)? 
                                        <Link to="#" onClick={()=>removeItem('character', item)}>
                                          <i className="mdi mdi-heart text-warning"></i>
                                        </Link>
                                        :
                                        <Link to="#" onClick={()=>addItem('character', {...item, title: item.name})}>
                                          <i className="mdi mdi-heart-outline"></i>
                                        </Link>
                                    }
                                  </div>
                                  <Link to={"character/" + item.id}>
                                  <img
                                    src={item.thumbnail}
                                    alt={item.name}
                                    className="img-fluid mx-auto d-block"
                                  />
                                      </Link>
                                </div>

                                <div className="text-center">
                                  <p className="font-size-12 mb-1">
                                    {item.name}
                                  </p>
                                  <h5 className="font-size-15">
                                    <Link to="#" className="text-dark">
                                      {item.description}
                                    </Link>
                                  </h5>
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                      </Row>
                    </InfiniteScroll>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Characters;
