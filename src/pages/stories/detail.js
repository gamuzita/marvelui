import React from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col,
  Table,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useParams } from "react-router-dom";
import { addItem, removeItem, verifyItem } from "./../../localstorage";

export const GET_STORY_DETAIL = gql`
  query getDetail($id: ID!) {
    story(id: $id) {
        id
        title
        description
        thumbnail
        
      characters {
        id
      name
      description
      thumbnail
      }
      comics {
        id
        title
        description
        thumbnail
        issueNumber
        format
      }
    }
  }
`;

const Story = () => {
  const { id } = useParams();
  const { loading, data } = useQuery(GET_STORY_DETAIL, {
    variables: { id },
  });

  if (loading) {
    return <div>Loading...</div>;
  }

  const { story } = data;

  console.log(story);
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <Card>
                <CardBody>
                  <Row>
                    <Col xl={5}>
                      <div className="product-detail">
                        <Row>
                          <Col md={12} xs={12}>
                            <div className="product-img">
                              <img
                                src={
                                    story.thumbnail === ""
                                      ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                      : story.thumbnail
                                  }
                                id="expandedImg1"
                                alt={story.title}
                                className="img-fluid mx-auto d-block"
                              />
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col xl={7}>
                      <div className="mt-4 mt-xl-3">
                        <Link to="#" className="text-primary">
                          {story.title}
                        </Link>
                        <h5 className="mt-1 mb-3">{story.description}</h5>
                      </div>
                      <div className="text-center mt-2">
                        <Col sm={2}>
                          {verifyItem('story', story.id)? 
                            <Button onClick={()=>removeItem('story', story)} color="warning" block type="button" className="waves-effect waves-light">
                                <i className="mdi mdi-heart mr-1"></i>  
                            </Button>
                            :
                            <Button onClick={()=>addItem('story', story)} color="light" block type="button" className="waves-effect waves-light">
                                <i className="mdi mdi-heart mr-1"></i>
                            </Button>
                            }
                        </Col>
                    </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={6}>
                      <Card className="checkout-order-summary">
                        <CardBody>
                          <div className="p-3 bg-light mb-4">
                            <h5 className="font-size-14 mb-0">Characters</h5>
                          </div>
                          <div className="table-responsive">
                            <Table className="table-centered mb-0 table-nowrap">
                              <tbody>
                                {story.characters.map((character, index) => {
                                  return (
                                    <tr key={index}>
                                      <th scope="row">
                                      <Link
                                            to={"/character/" + character.id}
                                            className="text-dark"
                                          >
                                        <img
                                          src={
                                            character.thumbnail === ""
                                              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                              : character.thumbnail
                                          }
                                          alt="product-img"
                                          title="product-img"
                                          className="avatar-md"
                                        />
                                        </Link>
                                      </th>
                                      <td>
                                        <h5 className="font-size-14 text-truncate">
                                          <Link
                                            to={"/character/" + character.id}
                                            className="text-dark"
                                          >
                                            {character.name}
                                          </Link>
                                        </h5>
                                        <p className="text-muted mb-0">
                                          {character.description}
                                        </p>
                                      </td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </Table>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col lg={6}>
                      <Card className="checkout-order-summary">
                        <CardBody>
                          <div className="p-3 bg-light mb-4">
                            <h5 className="font-size-14 mb-0">Stories</h5>
                          </div>
                          <div className="table-responsive">
                            <Table className="table-centered mb-0 table-nowrap">
                              <tbody>
                                {story.comics.map((comic, index) => {
                                  return (
                                    <tr key={index}>
                                      <th scope="row">
                                      <Link
                                            to={"/comic/" + comic.id}
                                            className="text-dark"
                                          >
                                        <img
                                          src={
                                            comic.thumbnail === ""
                                              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                              : comic.thumbnail
                                          }
                                          alt="product-img"
                                          title="product-img"
                                          className="avatar-md"
                                        />
                                        </Link>
                                      </th>
                                      <td>
                                        <h5 className="font-size-14 text-truncate">
                                          <Link
                                            to={"/comic/" + comic.id}
                                            className="text-dark"
                                          >
                                            {comic.title}
                                          </Link>
                                        </h5>
                                        <p className="text-muted mb-0">
                                          {comic.description}
                                        </p>
                                      </td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </Table>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Story;
