import React from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col
} from "reactstrap";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import InfiniteScroll from "react-infinite-scroll-component";
import { addItem, removeItem, verifyItem } from "./../../localstorage";

export const GET_STORIES = gql`
  query getComics($offset: Int, $limit: Int) {
    stories(
      pagination: { offset: $offset, limit: $limit }
    )
    {
      offset
      total
      limit
      count
      results {
        id
        title
        description
        thumbnail
      }
    }
  }
`;

// export const GETSORTTYPE = gql`
//  enum SortType {
//   TITLE
//   ISSUE_NUMBER
// }
// `;

const LIMIT = 20;


const Stories = () => {
  const { loading, data, fetchMore } = useQuery(GET_STORIES, {
    variables: {
      limit: LIMIT
    },
  });

  if (loading) {
    return <div>loading</div>;
  }

  const loadMore = () => {
    fetchMore({
      variables: {
        offset: stories.length,
        limit: LIMIT
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        const prevStories = prev?.stories?.results || [];
        const newStories = fetchMoreResult?.stories?.results || [];

        const data = {
          stories: {
            ...fetchMoreResult.stories,
            results: [...prevStories, ...newStories],
          },
        };

        return data;
      },
    });
  };

  const stories = data?.stories?.results || [];

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={12}>
              <Card>
                <CardBody>
                  <div>

                    <InfiniteScroll
                      dataLength={stories.length}
                      next={loadMore}
                      hasMore={stories.length < data?.stories?.total}
                      loader={<div>loading...</div>}
                      height={"72vh"}
                    >
                      <Row className="no-gutters">
                        {stories.map((item, index) => {
                          return (
                            <Col xl={4} sm={6} key={index}>
                              <div className="product-box">
                                <div className="product-img">
                                  <div className="product-like">
                                  {
                                      verifyItem('story', item.id)? 
                                        <Link to="#" onClick={()=>removeItem('story', item)}>
                                          <i className="mdi mdi-heart text-warning"></i>
                                        </Link>
                                        :
                                        <Link to="#" onClick={()=>addItem('story', item)}>
                                          <i className="mdi mdi-heart-outline"></i>
                                        </Link>
                                    }
                                  </div>
                                  <Link to={"story/" + item.id}>
                                  <img
                                    src={
                                      item.thumbnail === ""
                                        ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                        : item.thumbnail
                                    }
                                    alt={item.title}
                                    className="img-fluid mx-auto d-block"
                                  />
                                   </Link>
                                </div>

                                <div className="text-center">
                                  <p className="font-size-12 mb-1">
                                    {item.title}
                                  </p>
                                  <h5 className="font-size-15">
                                    <Link to="#" className="text-dark">
                                      {item.description}
                                    </Link>
                                  </h5>
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                      </Row>
                    </InfiniteScroll>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Stories;
