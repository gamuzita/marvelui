import React, { useState } from "react";
import {
  Container,
  Card,
  CardBody,
  Row,
  Col,
  Table
} from "reactstrap";
import { Link } from "react-router-dom";
import { getLocalStorage, removeItem } from "./../../localstorage";

const Favorites = () => {
  const [list, setlist] = useState(getLocalStorage());

  const removeFavorite = (type, item) => {
    removeItem(type, item);
    setTimeout(function () {
      setlist(getLocalStorage());
    }, 500);
  };


  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg={4}>
              <Card>
                <CardBody>
                  <div className="p-3 bg-light mb-4">
                    <h5 className="font-size-14 mb-0">Favorite Characters</h5>
                  </div>
                  <div className="table-responsive">
                    <Table className="table-centered mb-0 table-nowrap">
                      <tbody>
                        {list.filter((c) => c.type === "character").map((item, key) => (
                          <tr key={key}>
                            <td>
                              <img
                                src={
                                  item.thumbnail === ""
                                    ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                    : item.thumbnail
                                }
                                alt="product-img"
                                title="product-img"
                                className="avatar-md"
                              />
                            </td>
                            <td>
                              <h5 className="font-size-14 text-truncate">
                                <Link
                                  to={"/character/" + item.id}
                                  className="text-dark"
                                >
                                  {item.title}
                                </Link>
                              </h5>
                              <Link
                                to="#"
                                onClick={() =>
                                  removeFavorite("character", item)
                                }
                                className="action-icon text-danger"
                              >
                                <i className="mdi mdi-trash-can font-size-18"></i>
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </CardBody>
              </Card>
            </Col>

            <Col lg={4}>
              <Card>
                <CardBody>
                  <div className="p-3 bg-light mb-4">
                    <h5 className="font-size-14 mb-0">Favorite Comics</h5>
                  </div>
                  <div className="table-responsive">
                    <Table className="table-centered mb-0 table-nowrap">
                      <tbody>
                        {list.filter((c) => c.type === "comic").map((item, key) => (
                          <tr key={key}>
                            <td>
                              <img
                                src={
                                  item.thumbnail === ""
                                    ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                    : item.thumbnail
                                }
                                alt="product-img"
                                title="product-img"
                                className="avatar-md"
                              />
                            </td>
                            <td>
                              <h5 className="font-size-14 text-truncate">
                                <Link
                                  to={"/comic/" + item.id}
                                  className="text-dark"
                                >
                                  {item.title}
                                </Link>
                              </h5>
                              <Link
                                to="#"
                                onClick={() => removeFavorite("comic", item)}
                                className="action-icon text-danger"
                              >
                                <i className="mdi mdi-trash-can font-size-18"></i>
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </CardBody>
              </Card>
            </Col>

            <Col lg={4}>
              <Card>
                <CardBody>
                  <div className="p-3 bg-light mb-4">
                    <h5 className="font-size-14 mb-0">Favorite Stories</h5>
                  </div>
                  <div className="table-responsive">
                    <Table className="table-centered mb-0 table-nowrap">
                      <tbody>
                        {list.filter((c) => c.type === "story").map((item, key) => (
                          <tr key={key}>
                            <td>
                              <img
                                src={
                                  item.thumbnail === ""
                                    ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                                    : item.thumbnail
                                }
                                alt="product-img"
                                title="product-img"
                                className="avatar-md"
                              />
                            </td>
                            <td>
                              <h5 className="font-size-14 text-truncate">
                                <Link
                                  to={"/story/" + item.id}
                                  className="text-dark"
                                >
                                  {item.title}
                                </Link>
                              </h5>
                              <Link
                                to="#"
                                onClick={() => removeFavorite("story", item)}
                                className="action-icon text-danger"
                              >
                                <i className="mdi mdi-trash-can font-size-18"></i>
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Favorites;
