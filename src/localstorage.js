const getLocalStorage = () => {
    let list = localStorage.getItem('list');
    if (list) {
      return (list = JSON.parse(localStorage.getItem('list')));
    } else {
      return [];
    }
  };

const addItem =(type, item)=>{
    let currentLC = getLocalStorage();
    if(currentLC){
        const exist = currentLC.find(c=> c.type===type && c.id === item.id);

        if(exist){
            removeItem(type, item, currentLC);
        }
        else{
            addItemToList(type, item, currentLC);
        }
    }
    else{
        addItemToList(type, item, currentLC);
    }

}

const addItemToList =(type, item, currentLC)=>{

    const newItem = {
        type : type,
        id : item.id,
        title: item.title,
        description: item.description,
        thumbnail:item.thumbnail
    }

    currentLC.push(newItem);

    setList(currentLC);
}

const removeItem =(type, item, currentLC)=>{
    console.log(!currentLC);
    if(!currentLC)
    {
        currentLC = getLocalStorage();
    }
    
    setList(currentLC.filter((c) => c.id !== item.id));
}

const setList =(list)=>{
    localStorage.setItem('list', JSON.stringify(list));
}

const verifyItem =(type, id)=>{
    let currentLC = getLocalStorage();

    if(currentLC){
        const exist = currentLC.find(c=> c.type===type && c.id === id);
        if(exist)
            return true;
        else
            return false;
    }
    else{
        return false;
    }
}

export {addItem, removeItem, verifyItem, getLocalStorage};