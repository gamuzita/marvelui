import React from "react";
import { Redirect } from "react-router-dom";

import Characters from "../pages/characters";
import Character from "../pages/characters/detail";
import Comics from "../pages/comics";
import Comic from "../pages/comics/detail";
import Stories from "../pages/stories";
import Story from "../pages/stories/detail";
import Favorites from "../pages/favorites";

const authProtectedRoutes = [
	
];

const publicRoutes = [

	{ path: "/characters", component: Characters },
	{ path: "/character/:id", component: Character },

	{ path: "/comics", component: Comics },
	{ path: "/comic/:id", component: Comic },

	{ path: "/stories", component: Stories },
	{ path: "/story/:id", component: Story },

	{ path: "/favorites", component: Favorites },

	// this route should be at the end of all other routes
	{ path: "/", exact: true, component: () => <Redirect to="/characters" /> }
];

export { authProtectedRoutes, publicRoutes };
